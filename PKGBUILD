# Maintainer  : Jean-Michel T.Dydak <jean-michel@obarun.org> <jean-michel@syntazia.org>
#--------------
## PkgSource  : https://www.archlinux.org/packages/extra/x86_64/postgresql/
## Maintainer: Levente Polyak <anthraxx[at]archlinux[dot]org>
## Maintainer: Dan McGee <dan@archlinux.org>
#--------------------------------------------------------------------------------------

pkgbase=postgresql
pkgname=('postgresql-libs' 'postgresql-docs' 'postgresql')
pkgver=11.2
pkgrel=5
arch=('x86_64')
license=('custom:PostgreSQL')
pkgdesc="Sophisticated object-relational DBMS"
url="https://www.postgresql.org/"
source=("https://ftp.postgresql.org/pub/source/v$pkgver/postgresql-$pkgver.tar.bz2"
    'postgresql-run-socket.patch'
    'postgresql-perl-rpath.patch'
    'postgresql.pam'
    'postgresql.logrotate'
    'postgresql-check-db-dir'
    'postgresql.sysusers.conf'
    'postgresql.tmpfiles.conf')

makedepends=(
    'krb5'
    'libxml2'
    'python'
    'python2'
    'perl'
    'tcl>=8.6.0'
    'libressl'
    'pam'
    'zlib'
    'icu'
    'libldap'
    'llvm'
    'clang')

#--------------------------------------------------------------------------------------
prepare() {
    cd "postgresql-$pkgver"

    patch -p1 < ../postgresql-run-socket.patch
    patch -p1 < ../postgresql-perl-rpath.patch
}

build() {
    cd "postgresql-$pkgver"

    local options=(
        --prefix=/usr
        --mandir=/usr/share/man
        --datadir=/usr/share/postgresql
        --sysconfdir=/etc
        --with-gssapi
        --with-libxml
        --with-openssl
        --with-perl
        --with-python
        --with-tcl
        --with-pam
        --with-system-tzdata=/usr/share/zoneinfo
        --with-uuid=e2fs
        --with-icu
        --without-systemd
        --without-selinux
        --with-ldap
        --with-llvm
        --enable-nls
        --enable-thread-safety
        --disable-rpath )

    ## only build plpython3 for now
    ./configure ${options[@]} PYTHON=/usr/bin/python
    make -C src/pl/plpython all
    make -C contrib/hstore_plpython all
    make -C contrib/ltree_plpython all

    ## save plpython3 build and Makefile.global
    cp -a src/pl/plpython{,3}
    cp -a contrib/hstore_plpython{,3}
    cp -a contrib/ltree_plpython{,3}
    cp -a src/Makefile.global{,.python3}
    make distclean

    ## regular build with everything
    ./configure ${options[@]} PYTHON=/usr/bin/python2
    make world
}

_postgres_check() {
    make "${1}" || (find . -name regression.diffs | \
     while read -r line; do
      error "make ${1} failure: ${line}"
      cat "${line}"
     done; exit 1)
}

check() {
    cd "postgresql-$pkgver"

    _postgres_check check
    _postgres_check check-world
}

package_postgresql-libs() {
    pkgdesc="Libraries for use with PostgreSQL"
    depends=(
        'krb5'
        'libressl'
        'readline>=6.0'
        'zlib'
        'libldap')
    provides=(
        'postgresql-client')
    conflicts=(
        'postgresql-client')

    cd "postgresql-$pkgver"

    ## install license
    install -Dm 644 COPYRIGHT -t "${pkgdir}/usr/share/licenses/${pkgname}"

    ## install libs and non-server binaries
    for dir in src/interfaces src/bin/pg_config src/bin/pg_dump src/bin/psql src/bin/scripts; do
     make -C ${dir} DESTDIR="${pkgdir}" install
    done

    for util in pg_config pg_dump pg_dumpall pg_restore psql \
     clusterdb createdb createuser dropdb dropuser pg_isready reindexdb vacuumdb; do
      install -Dm 644 doc/src/sgml/man1/${util}.1 "${pkgdir}"/usr/share/man/man1/${util}.1
    done

    cd src/include

    install -d "${pkgdir}"/usr/include/{libpq,postgresql/internal/libpq}

    ## these headers are needed by the public headers of the interfaces
    install -m 644 pg_config.h "${pkgdir}/usr/include"
    install -m 644 pg_config_os.h "${pkgdir}/usr/include"
    install -m 644 pg_config_ext.h "${pkgdir}/usr/include"
    install -m 644 postgres_ext.h "${pkgdir}/usr/include"
    install -m 644 libpq/libpq-fs.h "${pkgdir}/usr/include/libpq"
    install -m 644 pg_config_manual.h "${pkgdir}/usr/include"

    ## these he aders are needed by the not-so-public headers of the interfaces
    install -m 644 c.h "${pkgdir}/usr/include/postgresql/internal"
    install -m 644 port.h "${pkgdir}/usr/include/postgresql/internal"
    install -m 644 postgres_fe.h "${pkgdir}/usr/include/postgresql/internal"
    install -m 644 libpq/pqcomm.h "${pkgdir}/usr/include/postgresql/internal/libpq"
}

package_postgresql-docs() {
    pkgdesc="HTML documentation for PostgreSQL"
    options=(
        'docs')

    cd "postgresql-$pkgver"

    install -Dm 644 COPYRIGHT -t "${pkgdir}/usr/share/licenses/${pkgname}"

    make -C doc/src/sgml DESTDIR="${pkgdir}" install-html
    chown -R root:root "${pkgdir}/usr/share/doc/postgresql/html"

    ## clean up
    rmdir "${pkgdir}"/usr/share/man/man{1,3,7}
    rmdir "${pkgdir}"/usr/share/man
}

package_postgresql() {
    pkgdesc='Sophisticated object-relational DBMS'
    backup=(
        'etc/pam.d/postgresql'
        'etc/logrotate.d/postgresql')
    depends=(
        "postgresql-libs>=${pkgver}"
        'krb5'
        'libxml2'
        'readline>=6.0'
        'libressl'
        'pam'
        'icu'
        'libldap')
    optdepends=(
        'python2: for PL/Python 2 support'
        'python: for PL/Python 3 support'
        'perl: for PL/Perl support'
        'tcl: for PL/Tcl support'
        'llvm: for JIT compilation support'
        'postgresql-old-upgrade: upgrade from previous major version using pg_upgrade')
    options=(
        'staticlibs')

    cd "postgresql-$pkgver"

    ## install
    make DESTDIR="${pkgdir}" install
    make -C contrib DESTDIR="${pkgdir}" install
    make -C doc/src/sgml DESTDIR="${pkgdir}" install-man

    ## install plpython3
    mv src/Makefile.global src/Makefile.global.save
    cp src/Makefile.global.python3 src/Makefile.global
    touch -r src/Makefile.global.save src/Makefile.global
    make -C src/pl/plpython3 DESTDIR="${pkgdir}" install
    make -C contrib/hstore_plpython3 DESTDIR="${pkgdir}" install
    make -C contrib/ltree_plpython3 DESTDIR="${pkgdir}" install

    ## we don't want these, they are in the -libs package
    for dir in src/interfaces src/bin/pg_config src/bin/pg_dump src/bin/psql src/bin/scripts; do
     make -C ${dir} DESTDIR="${pkgdir}" uninstall
    done
    for util in pg_config pg_dump pg_dumpall pg_restore psql \
      clusterdb createdb createuser dropdb dropuser pg_isready reindexdb vacuumdb; do
     rm "${pkgdir}"/usr/share/man/man1/${util}.1
    done

    install -Dm 644 COPYRIGHT -t "${pkgdir}/usr/share/licenses/${pkgname}"

    cd "${srcdir}"
    install -Dm 755 postgresql-check-db-dir -t "${pkgdir}/usr/bin"

    install -Dm 644 ${pkgname}.pam "${pkgdir}/etc/pam.d/${pkgname}"
    install -Dm 644 ${pkgname}.logrotate "${pkgdir}/etc/logrotate.d/${pkgname}"

    install -Dm 644 postgresql.sysusers.conf "${pkgdir}/usr/lib/sysusers.d/${pkgname}.conf"
    install -Dm 644 postgresql.tmpfiles.conf "${pkgdir}/usr/lib/tmpfiles.d/${pkgname}.conf"
  
    ## clean up unneeded installed items
    rm -rf "${pkgdir}/usr/include/postgresql/internal"
    rm -rf "${pkgdir}/usr/include/libpq"

    #find "${pkgdir}/usr/include" -maxdepth 1 -type f -execdir rm {} +
    for files in $(find ${pkgdir}/usr/include/ -name \*.h); do rm -f $files; done
    rmdir "${pkgdir}/usr/share/doc/postgresql/html"
}

#--------------------------------------------------------------------------------------
sha512sums=('dae00a543fdeed36bc338abd4ccfd9fe9a8b6b2b7eaa00b1078e4f27802de75a461c27da2800bc9dd64e658681e5787bd03764bde79940656cae1e8c8716f011'
            '031efe12d18ce386989062327cdbbe611c5ef1f94e4e1bead502304cb3e2d410af533d3c7f1109d24f9da9708214fe32f9a10ba373a3ca8d507bdb521fbb75f7'
            '38302242b30c01c7981574ed28d9cbd9dc73bf6b56ba3a032afb5d0885ae83e5aa72ce578bf2422214dfa6c46f09d0bdd7cccaeb3c25d58754eb1a34f8bf5615'
            '1e6183ab0eb812b3ef687ac2c26ce78f7cb30540f606d20023669ac00ba04075487fb72e4dc89cc05dab0269ff6aca98fc1167cc75669c225b88b592482fbf67'
            'c9ecd3b5fb2b6220bdf0efb322007f2321b64cb5d4ca63b9e5f3e5f05f4b05481f69605dfbb9c4e8dc2fb0c645c56ed3eafec428aeacc2a820644570e4703cf5'
            '73af1cd31638791f81aa2623d51188364107a57b55e4deba6691cd99e96ae5ea0dd94b25a0e95d9e21ac64f36f71919a05cd31233c754bde025215a5a02e055c'
            '36f7a5d38370fdc4d4267fd5a8a8330f152a1077bf0f065b89d4a7b8112ccd42be2c46c863791b77de02013f28275a42219f4236e7cb837c3f8cfd5fcc7d3373'
            '27094b07fd57a077da9cb31e1970998766e7aff5bd7a2c4545fe3a5a96e84ecc6e5c541f418b2f395c06404fa29a17d9f88db0f4efdd392a02ac029662697619')
